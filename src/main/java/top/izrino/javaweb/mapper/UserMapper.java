package top.izrino.javaweb.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import top.izrino.javaweb.entity.User;

@Mapper
public interface UserMapper {

    @Select("select * from user where id =#{id};")
    User getUserById(Long id);

    @Select("select * from user where username =#{username};")
    User getUserByUserName(String username);

    @Select("select * from user where username =#{username} and password=#{password};")
    User getUser(String username, String password);

    //根据id删除用户
    @Delete("delete from user where id =#{id}")
    int deleteUser(Long id);

    //新增用户
    @Insert("insert into user(id,username,password,name,number)values(#{id},#{username},#{password},#{name},#{number})")
    int addUser(User user);
}
