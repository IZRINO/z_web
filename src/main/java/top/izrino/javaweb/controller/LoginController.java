package top.izrino.javaweb.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import top.izrino.javaweb.common.R;
import top.izrino.javaweb.entity.User;
import top.izrino.javaweb.service.UserService;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Api(tags = "用户接口")
@RestController
@RequestMapping("/login")
@ResponseBody
public class LoginController {
    private final UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation("登录方法")
    @PostMapping()
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "登录者用户名", required = true),
            @ApiImplicitParam(name = "password", value = "登录者密码", required = true),
    })
    public R<User> login(String username, String password, HttpServletRequest request){
        String passwd = DigestUtils.md5DigestAsHex(password.getBytes());

        User user = userService.getUser(username, passwd);

        if (user!=null){
            user.setPassword("");
            return R.success("登录成功",user);
        }

        return R.error("用户名或密码不匹配");
    }

    @ApiOperation("注册方法")
    @PostMapping("/register")
    public R<String> register(@RequestBody User user) {
        user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));

        int i = userService.addUser(user);
        log.info("i:" + i);
        return R.success("注册成功");
    }

    @ApiOperation("根据用户id查找")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户id", required = true),
    })
    @GetMapping("/getUserById")
    public R<User> getUser(Long id){
        User user = userService.getUser(id);
        return R.success(user);
    }
    @ApiOperation("根据用户名查找")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "登录者用户名", required = true),
    })
    @GetMapping("/getUserByUsername")
    public R<User> getUser(String username){
        User user = userService.getUser(username);
        return R.success(user);
    }
}
