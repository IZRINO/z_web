package top.izrino.javaweb.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serial;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@ApiModel(value = "R<T>", description = "返回类")
public class R<T> implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("状态码:200成功，0和其它数字为失败")
    private Integer code; //编码：1成功，0和其它数字为失败
    @ApiModelProperty("消息")
    private String msg; //错误信息
    @ApiModelProperty("数据")
    private T data; //数据
    private Map<Object, Object> map = new HashMap<>(); //动态数据

    public static <T> R<T> success(T object) {
        R<T> r = new R<>();
        r.data = object;
        r.code = 200;
        return r;
    }


    public static <T> R<T> success(String msg, T object) {
        R<T> r = new R<>();
        r.data = object;
        r.msg = msg;
        r.code = 200;
        return r;
    }

    public static <T> R<T> success(String msg) {
        R<T> r = new R<>();
        r.msg = msg;
        r.code = 200;
        return r;
    }

    public static <T> R<T> error(String msg) {
        R<T> r = new R<>();
        r.msg = msg;
        r.code = 0;
        return r;
    }

    public static <T> R<T> error(String msg, T object) {
        R<T> r = new R<>();
        r.msg = msg;
        r.data = object;
        r.code = 0;
        return r;
    }

    public R<T> add(String key, Object value) {
        this.map.put(key, value);
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Map<Object, Object> getMap() {
        return map;
    }

    public void setMap(Map<Object, Object> map) {
        this.map = map;
    }
}