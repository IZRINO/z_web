package top.izrino.javaweb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.izrino.javaweb.entity.User;
import top.izrino.javaweb.mapper.UserMapper;

@Service
public class UserService {

    private final UserMapper userMapper;

    @Autowired
    public UserService(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    public User getUser(Long id) {
        return userMapper.getUserById(id);
    }

    public User getUser(String username) {
        return userMapper.getUserByUserName(username);
    }

    public User getUser(String username, String password) {
        return userMapper.getUser(username, password);
    }

    public int addUser(User user) {
        return userMapper.addUser(user);
    }

    public int deleteUser(Long id) {
        return userMapper.deleteUser(id);
    }
}
